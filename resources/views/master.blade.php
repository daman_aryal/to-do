<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>My Gallary Apps</title>
<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
</head>
<body>
    <div class="container">
        <legend><h1 class="text-center">To Do List</h1></legend>
        <div class="nav"></div>
        @yield('content')
    </div>
</body>
</html>