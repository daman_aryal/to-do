@extends('master')
@section('content')
    @foreach($errors->all() as $error)
    <p>{{$error}}</p>
    @endforeach
    <legend>Please Register Your New Account</legend>
    {!!Form::open()!!}
       <div class="form-group">
        <input type="text" name="name" placeholder="Enter your username"/>
       </div>
       <div class="form-group">
        <input type="password" name="password" placeholder="Enter your password"/>
       </div>

       <input type="submit" value="Register" class="btn btn-primary"/>
    {!!Form::close()!!}
@endsection