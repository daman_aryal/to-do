    @extends('master')
           @section('content')
               <h1>welocme to home</h1>
               <legend>Your Items <small><a href="{{url('info/new')}}">New Iteams</a></small></legend>
               <span>{{session('msg')}}</span>
            <ul>
                @foreach($orders as $order)
                    <li style="list-style: none">
                        {!!Form::open()!!}
                            <input type="checkbox"
                            onclick="this.form.submit()"
                             {{$order->done ? 'checked' : ''}}/>
                             <input type="hidden"
                                name="id"
                                value="{{$order->id}}"
                             />
                            {{$order->name}} <span><a href="{{url('info/delete/'.$order->id)}}">X</a></span>

                        {!!Form::close()!!}
                    </li>
                @endforeach
            </ul>
            <a href="{{url('info/logout')}}" class="btn btn-primary">Log Out</a>
    @endsection