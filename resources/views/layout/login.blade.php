@extends('master')
@section('content')


   <legend><a href="{{url('info/register')}}" class="btn btn-default">Register Your Account</a></legend>
    {!!Form::open()!!}
           <div class="row">
           <div class="col-md-4"></div>
                <div class="col-md-4">
                           @foreach($errors->all() as $error)
                              <p style="color: red">{{$error}}</p>
                           @endforeach
                           <div class="form-group">
                              {!!Form::text('username','',array('class'=>'form-control', 'placeholder'=>'name'))!!}
                          </div>
                           <div class="form-group">
                            {!!Form::password('password', array('class'=>'form-control', 'placeholder'=>'password'))!!}
                           </div>
                           {!!Form::submit('Login', array('class'=>'btn btn-primary'))!!}
                </div>
           </div>
           <div class="col-md-4"></div>
    {!!Form::close()!!}
@endsection