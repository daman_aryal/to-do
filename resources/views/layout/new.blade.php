@extends('master')
@section('content')
    <legend>Add your new Task</legend>
    {!!Form::open()!!}
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
        <input type="text" name="task" placeholder="Enter your task" />
        <input type="submit" value="New task"/>
    {!!Form::close()!!}
@endsection