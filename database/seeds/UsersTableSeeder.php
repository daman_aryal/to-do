<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $users=array(
            'name'=>'Daman',
            'password'=>Hash::make('hpkolaptop')

        );

        DB::table('users')->insert($users);
    }
}
