<?php

use Illuminate\Database\Seeder;

class IteamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->delete();

        $todos=array(
            array(
            'owner_id'=>1,
            'name'=>'pick up milk',
            'done'=>false
        ),

        array(
            'owner_id'=>1,
            'name'=>'walk the dog',
            'done'=>true
        ),

            array(
                'owner_id'=>1,
                'name'=>'cook dinner',
                'done'=>true
            )

    );

    DB::table('todos')->insert($todos);
    }
}
