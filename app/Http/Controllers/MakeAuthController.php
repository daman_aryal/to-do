<?php
/**
 * Created by PhpStorm.
 * User: manmi4
 * Date: 11/12/2015
 * Time: 9:17 AM
 */

namespace App\Http\Controllers;

use App\Todo;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MakeAuthController extends Controller{



    public function getLogin(){
        return view('layout.login');
    }


    public function getHome(){
        $iteams=Auth::user();
        $orders=$iteams->todo;

        /*foreach($orders as $order){
            echo $order->name;
        }*/
       return view('layout.home', array(
            'orders'=>$orders
        ));

    }

    public function postHome()
    {

       $id=Input::get('id');
        $userid=Auth::user()->id;
       $iteam=Todo::find($id);

       if($iteam->owner_id == $userid){
           $iteam->mark();
       }

        return Redirect::to('info/home');

    }

    public function postLogin()
    {
        $input=Input::all();

        $rules=array(
            'username'=>'required',
            'password'=>'required'
        );

        $v=Validator::make($input, $rules);

        if($v->fails()){
            return Redirect::to('info/login')->withErrors($v);
        }else{
            $crediental=array(
                'name' => $input['username'],
                'password' => $input['password']
            );

            if(Auth::attempt($crediental)){
                return Redirect::to('info/home');
            }else{
                return Redirect::to('info/login')->withErrors(
                    array(
                        'wrong user name or password entered'
                    )
                );
            }


         }
    }

    public function getNew()
    {
        return view('layout.new');
    }

    public function postNew(){
        $tasks=Input::all();

        $rules=array(
            'task'=>'required|min:3|max:255'
        );

        $v=Validator::make($tasks, $rules);

        if($v->fails()){
            return Redirect::to('info/new')->withErrors($v);
        }else{
            $todo=new Todo();

            $todo->owner_id=Auth::user()->id;
            $todo->name=Input::get('task');
            $todo->save();

            return Redirect::to('info/home');

        }
    }

    public function getDelete($id){
        $tast=Todo::find($id);

        $tast->delete();

        return Redirect::to('info/home')->with('msg','Successfully Deleted');
    }

    public function getLogout(){
        Auth::logout();
        return Redirect::to('info/login');
    }

    public function getRegister()
    {
      return view('layout.register');
    }

    public function postRegister(){
        $registers=Input::all();

        $rules=array(
            'username'=>'required',
            'password'=>'required'
        );

        $v=Validator::make($registers, $rules);

        if($v->fails()){
            return Redirect::to('info/register')->withErrors(
                array(
                    'shit'
                )
            );
        }else{

            $user=new User();

            $password=$registers['password'];
            $hashpassword=Hash::make($password);

            $user->name = $registers['name'];
            $user->password=$hashpassword;
            $user->save();

            return Redirect::to('info/login')->with(
                array(
                    'your accoutn has been created'
                )
            );
        }
    }
}